package com.example.rafael.isspasses.net;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;

import java.util.HashMap;

/**
 * Created by Rafael on 03/12/2017.
 */

public class ParamsFactory {
    @NonNull
    public static HashMap<String, String> getPassesListParams(String latitude, String longitude) {

        HashMap<String, String> params = new HashMap<>();
        params.put("lat", latitude);
        params.put("lon", longitude);
        params.put("n", "20");
        return params;
    }
}

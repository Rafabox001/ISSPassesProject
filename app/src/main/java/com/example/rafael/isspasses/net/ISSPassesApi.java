package com.example.rafael.isspasses.net;

import com.example.rafael.isspasses.net.response.SimplePassesResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by Rafael on 03/12/2017.
 */

public interface ISSPassesApi {

    @GET("iss-pass.json")
    Call<SimplePassesResponse> getISSPassesList(@QueryMap Map<String,String> params);
}


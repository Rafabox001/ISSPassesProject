package com.example.rafael.isspasses.net.response;

/**
 * Created by Rafael on 03/12/2017.
 */

public class BaseResponse {
    public static final String SUCCESS  = "success";
    public static final String FAILURE  = "failure";

    public String message = "";

    @Override
    public String toString() {
        return "BaseResponse{" +
                "msg='" + message + '\'' +
                '}';
    }

    public boolean isSuccessfull(){
        return message.equals(SUCCESS);
    }

}

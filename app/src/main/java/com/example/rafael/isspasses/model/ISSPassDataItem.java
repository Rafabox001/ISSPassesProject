package com.example.rafael.isspasses.model;

/**
 * Created by Rafael on 03/12/2017.
 */

public class ISSPassDataItem {
    public String duration;
    public String risetime;

    @Override
    public String toString() {
        return "ISSPassDataItem{" +
                "duration='" + duration + '\'' +
                ", cirisetimety='" + risetime + '\'' +
                '}';
    }
}

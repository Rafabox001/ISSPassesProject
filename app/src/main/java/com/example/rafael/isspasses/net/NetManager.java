package com.example.rafael.isspasses.net;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Rafael on 03/12/2017.
 */

public class NetManager {
    private static final String BASE_URL = "http://api.open-notify.org/";

    private static ISSPassesApi issPassesApi;

    public static ISSPassesApi getApiInstance(){
        if (issPassesApi == null) {
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .writeTimeout(120,TimeUnit.SECONDS)
                    .readTimeout(120,TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            issPassesApi = retrofit.create(ISSPassesApi.class);
        }

        return issPassesApi;
    }
}

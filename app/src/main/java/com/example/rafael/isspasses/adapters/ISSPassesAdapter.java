package com.example.rafael.isspasses.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rafael.isspasses.R;
import com.example.rafael.isspasses.model.ISSPassDataItem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Rafael on 03/12/2017.
 */

public class ISSPassesAdapter extends RecyclerView.Adapter<ISSPassesAdapter.MyViewHolder> {

    private List<ISSPassDataItem> issPassesList;
    private Context mCotext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.duration)TextView duration;
        @BindView(R.id.risetime)TextView riseTime;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public ISSPassesAdapter(Context context) {
        this.mCotext = context;
        issPassesList = new ArrayList<>();
    }

    public void refreshRecyclerView(List<ISSPassDataItem> issData){
        issPassesList.clear();
        issPassesList.addAll(issData);

        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.iss_pass_data_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ISSPassDataItem issPassData = issPassesList.get(position);
        holder.duration.setText(issPassData.duration);
        //Convert Unix timestamp provided by the service to a more readable date
        Long seconds = Long.parseLong(issPassData.risetime);
        Date tranformedDate = new Date(seconds * 1000);

        // Create an instance of SimpleDateFormat used for formatting
        // the string representation of date (month/day/year)
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        // Using DateFormat format method we can create a string
        // representation of a date with the defined format.
        String issPassDate = df.format(tranformedDate);
        holder.riseTime.setText(issPassDate);
    }

    @Override
    public int getItemCount() {
        return issPassesList.size();
    }
}

package com.example.rafael.isspasses.net.response;

import com.example.rafael.isspasses.model.ISSPassDataItem;

import java.util.ArrayList;

/**
 * Created by Rafael on 03/12/2017.
 */

public class SimplePassesResponse extends BaseResponse {

    public ArrayList<ISSPassDataItem> response;

}
